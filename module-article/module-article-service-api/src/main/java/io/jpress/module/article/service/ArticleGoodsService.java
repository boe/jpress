package io.jpress.module.article.service;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import io.jpress.module.article.model.Article;
import io.jpress.module.article.model.ArticleGoods;


import java.util.List;

public interface ArticleGoodsService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public ArticleGoods findById(Object id);


    /**
     * find all model
     *
     * @return all <ArticleGoods
     */
    public List<ArticleGoods> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(ArticleGoods model);


    /**
     * save model to database
     *
     * @param model
     * @return
     */
    public boolean save(ArticleGoods model);


    /**
     * save or update model
     *
     * @param model
     * @return if save or update success
     */
    public boolean saveOrUpdate(ArticleGoods model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(ArticleGoods model);


    /**
     * 分页
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Page<? extends Model> paginate(int page, int pageSize);


    public void join(Page<? extends Model> page, String joinOnField);

    public void join(Page<? extends Model> page, String joinOnField, String[] attrs);

    public void join(Page<? extends Model> page, String joinOnField, String joinName);

    public void join(Page<? extends Model> page, String joinOnField, String joinName, String[] attrs);

    public void join(List<? extends Model> models, String joinOnField);

    public void join(List<? extends Model> models, String joinOnField, String[] attrs);

    public void join(List<? extends Model> models, String joinOnField, String joinName);

    public void join(List<? extends Model> models, String joinOnField, String joinName, String[] attrs);

    public void join(Model model, String joinOnField);

    public void join(Model model, String joinOnField, String[] attrs);

    public void join(Model model, String joinOnField, String joinName);

    public void join(Model model, String joinOnField, String joinName, String[] attrs);

    public void keep(Model model, String... attrs);

    public void keep(List<? extends Model> models, String... attrs);


    /**
     * 根据用户id分页查询
     * @param page
     * @param pagesize
     * @param userId
     * @return
     */
    Page<ArticleGoods> _paginateByUserId(int page, int pagesize, Long userId);

    /**
     * 关联商品
     * @param articleId
     * @param goodsIds
     */
    void linkArticleGoods(Long articleId, Long[] goodsIds);


    /**
     * 根据articleId查询商品
     * @param articleId
     * @return
     */
    List<ArticleGoods> findByArticleId(Long articleId);


    Page<ArticleGoods> _paginate(int page,int pagesize,String keyword);

    public boolean deleteByIds(Object... ids);

    Page<ArticleGoods> _paginateByArticleId(int page,int pagesize,Long articleId,String keyword);

}
