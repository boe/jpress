package io.jpress.module.article.service.provider;

import io.jboot.aop.annotation.Bean;

import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jpress.module.article.model.Banner;
import io.jpress.module.article.service.BannerService;

import javax.inject.Singleton;
import java.util.List;

@Bean
@Singleton
public class BannerServiceProvider extends JbootServiceBase<Banner> implements BannerService {
    @Override
    public List<Banner> findListByType(String type) {
        return DAO.findListByColumns(Columns.create("type", type), "order_number asc,id desc");
    }
}