package io.jpress.module.article.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jpress.module.article.model.ArticleLike;
import io.jpress.module.article.service.ArticleLikeService;

import javax.inject.Singleton;

@Bean
@Singleton
public class ArticleLikeServiceProvider extends JbootServiceBase<ArticleLike> implements ArticleLikeService {
    @Override
    public ArticleLike find(Long articleId, Long userId) {
        Columns columns = new Columns();
        columns.add("article_id",articleId);
        columns.add("user_id",userId);
        return DAO.findFirstByColumns(columns);
    }
}
