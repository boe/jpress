package io.jpress.module.article.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jpress.module.article.model.UserSignLog;
import io.jpress.module.article.service.UserSignLogService;

import javax.inject.Singleton;

@Bean
@Singleton
public class UserSignLogServiceProvider extends JbootServiceBase<UserSignLog> implements UserSignLogService {
    @Override
    public UserSignLog findUserSign(Long userId, String dateStr) {
        Columns columns = new Columns();
        columns.add("user_id",userId);
        columns.add("date_str",dateStr);
        return DAO.findFirstByColumns(columns);
    }
}