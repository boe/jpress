package io.jpress.module.article.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.aop.annotation.Bean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jpress.commons.utils.SqlUtils;
import io.jpress.module.article.model.Article;
import io.jpress.module.article.model.ArticleGoods;
import io.jpress.module.article.service.ArticleGoodsService;
import io.jpress.service.UserService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Bean
@Singleton
public class ArticleGoodsServiceProvider extends JbootServiceBase<ArticleGoods> implements ArticleGoodsService {
    @Inject
    private UserService userService;

    @Override
    public boolean deleteByIds(Object... ids) {
        for (Object id : ids) {
            deleteById(id);
        }
        return true;
    }


    @Override
    public Page<ArticleGoods> _paginateByUserId(int page, int pagesize, Long userId) {
        Columns columns = new Columns();
        columns.add("user_id",userId);
        columns.add("del_flag","0");
        return DAO.paginateByColumns(page, pagesize, columns, "id desc");
    }

    @Override
    public Page<ArticleGoods> _paginate(int page,int pagesize,String keyword){
        Columns columns = new Columns();
        columns.likeAppendPercent("title", keyword);

        Page<ArticleGoods> p = DAO.paginateByColumns(
                page,
                pagesize,
                columns,
                "id desc");
        userService.join(p,"user_id");
        return p;
    }


    @Override
    public Page<ArticleGoods> _paginateByArticleId(int page,int pagesize,Long articleId,String keyword){
        StringBuilder sqlBuilder = new StringBuilder("from article_goods g inner join article_goods_mapping m on(g.id=m.goods_id) ");

        Columns columns = new Columns();
        columns.add("m.article_id",articleId);
        SqlUtils.likeAppend(columns, "g.title", keyword);
        SqlUtils.appendWhereByColumns(columns, sqlBuilder);
        sqlBuilder.append(" order by id desc ");
        Page<ArticleGoods> p = DAO.paginate(page, pagesize, "select g.* ", sqlBuilder.toString(), columns.getValueArray());
        userService.join(p,"user_id");
        return p;
    }


    @Override
    public void linkArticleGoods(Long articleId, Long[] goodsIds) {
        Db.tx(() -> {
            Db.update("delete from article_goods_mapping where article_id = ?", articleId);

            if (goodsIds != null && goodsIds.length > 0) {
                List<Record> records = new ArrayList<>();
                for (Long goodsId : goodsIds) {
                    if(goodsId == null){
                        continue;
                    }
                    Record record = new Record();
                    record.set("article_id", articleId);
                    record.set("goods_id", goodsId);
                    records.add(record);
                }
                Db.batchSave("article_goods_mapping", records, records.size());
            }
            return true;
        });
    }

    @Override
    public List<ArticleGoods> findByArticleId(Long articleId) {
        return DAO.find("select a.* from article_goods a left join article_goods_mapping m on(a.id=m.goods_id) where m.article_id=? order by id asc ",articleId);
    }
}
