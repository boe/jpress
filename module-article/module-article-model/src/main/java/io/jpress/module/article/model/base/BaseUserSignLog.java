package io.jpress.module.article.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUserSignLog<M extends BaseUserSignLog<M>> extends JbootModel<M> implements IBean {

	public void setId(Long id) {
		set("id", id);
	}
	
	public Long getId() {
		return getLong("id");
	}

	public void setUserId(Long userId) {
		set("user_id", userId);
	}
	
	public Long getUserId() {
		return getLong("user_id");
	}

	public void setDateStr(String dateStr) {
		set("date_str", dateStr);
	}
	
	public String getDateStr() {
		return getStr("date_str");
	}

	public void setCreated(java.util.Date created) {
		set("created", created);
	}
	
	public java.util.Date getCreated() {
		return get("created");
	}

}
