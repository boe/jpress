/**
 * Copyright (c) 2016-2019, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the GNU Lesser General Public License (LGPL) ,Version 3.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jpress.web.api;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.Ret;
import io.jboot.utils.StrUtils;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.commons.sms.NeteaseSmsUtils;
import io.jpress.model.User;
import io.jpress.service.UserService;
import io.jpress.web.base.ApiControllerBase;

import javax.inject.Inject;
import java.io.IOException;

/**
 * 用户相关的API
 */
@RequestMapping("/api/user")
public class UserApiController extends ApiControllerBase {

    @Inject
    private UserService userService;

    /**
     * 获取用户信息
     */
    public void index() {

        Long id = getParaToLong("id");

        if (id == null) {
            renderFailJson();
            return;
        }

        User user = userService.findById(id);
        renderJson(Ret.ok().set("user", user));
    }


    /**
     * 获取登录用户自己的信息
     */
    public void me() {
        User user = getLoginedUser();
        if (user == null) {
            renderFailJson(1, "user not logined");
            return;
        }

        User result = userService.findById(user.getId());
        result.keepSafe();
        renderJson(Ret.ok().set("user", result));
    }


    public void save() {
        User user = getRawObject(User.class);

        if (user == null) {
            renderFailJson(1, "can not get user data");
            return;
        }

        user.keepUpdateSafe();
        userService.saveOrUpdate(user);

        renderOk();
    }


    public void sendCode(){

        String mobile = getPara("mobile");

        try {
            String result = NeteaseSmsUtils.sendCode(mobile);
            JSONObject jsonObject = JSONObject.parseObject(result);
            String code = jsonObject.getString("code");
            String msg = jsonObject.getString("msg");
            if(NeteaseSmsUtils.SUCCESS_CODE.equals(code)){
                renderOk();
            }else {
                renderFailJson(msg);
            }
        } catch (IOException e) {
            renderFailJson();
        }

    }


    /**
     * 绑定手机号
     */
    public void bindMobile() {
        User user = getLoginedUser();
        if (user == null) {
            renderFailJson(1, "user not logined");
            return;
        }
        String mobile = getPara("mobile");

        if(StrUtils.isBlank(mobile)){
            renderFailJson(1, "手机号不能为空。");
            return;
        }

        User bindedUser = userService.findFistByMobile(mobile);
        if(bindedUser != null){
            renderFailJson(1, "该手机号已经绑定过。");
            return;
        }
        String code = getPara("code");
        try {
            String result = NeteaseSmsUtils.verifyCode(mobile,code);
            JSONObject jsonObject = JSONObject.parseObject(result);
            String resultCode = jsonObject.getString("code");
            if(NeteaseSmsUtils.SUCCESS_CODE.equals(resultCode)){
                User updateUser = userService.findById(user.getId());
                updateUser.setMobile(mobile);
                updateUser.setMobileStatus(User.STATUS_OK);
                if(userService.update(updateUser)){
                    renderOk();
                    return;
                }
            }
            renderFailJson();
        } catch (IOException e) {
            renderFailJson();
        }

    }



}
