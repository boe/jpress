package io.jpress.commons.sms;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 网易云信短信
 */
public class NeteaseSmsUtils {

    //发送验证码的请求路径URL
    private static final String  SEND_CODE_URL = "https://api.netease.im/sms/sendcode.action";

    private static final String VERIFY_CODE_URL = "https://api.netease.im/sms/verifycode.action";

    //网易云信分配的账号，请替换你在管理后台应用下申请的Appkey
    private  static final String APP_KEY="963ac3a81f05f51b2600da74d26b34fb";

    //网易云信分配的密钥，请替换你在管理后台应用下申请的appSecret
    private static final String APP_SECRET="07b37f91e51c";

    //随机数
    private static final String NONCE="147896";

    //短信模板ID
    private static final String TEMPLATE_ID="3962515";

    //验证码长度，范围4～10，默认为4
    private static final String CODE_LEN="6";

    public static final String SUCCESS_CODE="200";

    public static String sendCode(String mobile) throws IOException {

        // 设置请求的的参数，requestBody参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        /*
         * 1.如果是模板短信，请注意参数mobile是有s的，详细参数配置请参考“发送模板短信文档”
         * 2.参数格式是jsonArray的格式，例如 "['13888888888','13666666666']"
         * 3.params是根据你模板里面有几个参数，那里面的参数也是jsonArray格式
         */
        nvps.add(new BasicNameValuePair("templateid", TEMPLATE_ID));
        nvps.add(new BasicNameValuePair("mobile", mobile));
        nvps.add(new BasicNameValuePair("codeLen", CODE_LEN));


        return sendRequest(SEND_CODE_URL,nvps);

    }


    private static String sendRequest(String url,List<NameValuePair> nvps) throws IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        String curTime = String.valueOf((new Date()).getTime() / 1000L);
        /*
         * 参考计算CheckSum的java代码，在上述文档的参数列表中，有CheckSum的计算文档示例
         */
        String checkSum = CheckSumBuilder.getCheckSum(APP_SECRET, NONCE, curTime);

        // 设置请求的header
        httpPost.addHeader("AppKey", APP_KEY);
        httpPost.addHeader("Nonce", NONCE);
        httpPost.addHeader("CurTime", curTime);
        httpPost.addHeader("CheckSum", checkSum);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");

        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

        // 执行请求
        HttpResponse response = httpClient.execute(httpPost);


        return EntityUtils.toString(response.getEntity(), "utf-8");
    }

    public static String verifyCode(String mobile,String code) throws IOException {
        // 设置请求的的参数，requestBody参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("mobile", mobile));
        nvps.add(new BasicNameValuePair("code", code));
        return sendRequest(VERIFY_CODE_URL,nvps);
    }



}
